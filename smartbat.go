// Package smartbat provides an interface to SMBus Smart Batteries.
package smartbat

import (
	"math"

	"github.com/go-daq/smbus"
)

type BatteryReg uint8

const (
	_                                 = iota
	RemainingCapacityAlarm BatteryReg = iota
	RemainingTimeAlarm
	BatteryMode
	AtRate
	AtRateTimeToFull
	AtRateTimeToEmpty
	AtRateOK
	Temperature
	Voltage
	Current
	AverageCurrent
	MaxError
	RelativeStateOfCharge
	AbsoluteStateOfCharge
	RemainingCapacity
	FullChargeCapacity
	RunTimeToEmpty
	AverageTimeToEmpty
	AverageTimeToFull
	ChargingCurrent
	ChargingVoltage
	BatteryStatus
	CycleCount
	DesignCapacity
	DesignVoltage
	SpecificationInfo
	ManufactureDate
	SerialNumber
)

// Proprietary UltraLife register code
const PackStatus BatteryReg = 0x2f

// BatteryStatus bits
const (
	ErrorCodeMask   = 0x03
	FullyDischarged = 1 << 4
	FullyCharged    = 1 << 5
	Discharging     = 1 << 6
	Initialized     = 1 << 7
)

// BatteryMode bits
const (
	InternalChargeController = 1 << 0
	PrimaryBatterySupport    = 1 << 1
	ConditionFlag            = 1 << 7
	ChargeControllerEnabled  = 1 << 8
	AlarmMode                = 1 << 13
	ChargerMode              = 1 << 14
	CapacityMode             = 1 << 15
)

// All batteries have a fixed SMBus (I2C) address
const I2CAddress uint8 = 0x0b

type SmartBattery struct {
	port *smbus.Conn
	addr uint8
}

// Return a new SmartBattery pointer
func New(bus int) (*SmartBattery, error) {
	port, err := smbus.Open(bus, I2CAddress)
	if err != nil {
		return nil, err
	}
	b := SmartBattery{port: port, addr: I2CAddress}
	return &b, nil
}

// Write a value to a battery register
func (b *SmartBattery) writeReg(code BatteryReg, val uint16) error {
	return b.port.WriteWord(b.addr, uint8(code), val)
}

// Read a value from a battery register
func (b *SmartBattery) readReg(code BatteryReg) (uint16, error) {
	return b.port.ReadWord(b.addr, uint8(code))
}

// Initialize a battery
func (b *SmartBattery) Initialize() error {
	mode, err := b.readReg(BatteryMode)
	if err == nil {
		// Disable ALARMs and CHARGER broadcasts from battery. A one
		// bit disables the feature.
		mode = mode | AlarmMode | ChargerMode
		// Report capacity in 10mWh units
		mode |= CapacityMode
		err = b.writeReg(BatteryMode, uint16(mode))
	}
	return err
}

// Return battery voltage in volts
func (b *SmartBattery) Voltage() (float32, error) {
	val, err := b.readReg(Voltage)
	if err != nil {
		return float32(math.NaN()), err
	}
	return float32(val) / 1000., nil
}

// Return battery current in amps. Negative current is flow out
// of the battery and positive current is flow in.
func (b *SmartBattery) Current() (float32, error) {
	val, err := b.readReg(Current)
	if err != nil {
		return float32(math.NaN()), err
	}
	return float32(int16(val)) / 1000., nil
}

// Return average current (in amps) over the past minute.
func (b *SmartBattery) AverageCurrent() (float32, error) {
	val, err := b.readReg(AverageCurrent)
	if err != nil {
		return float32(math.NaN()), err
	}
	return float32(int16(val)) / 1000., nil
}
