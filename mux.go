package smartbat

import (
	"github.com/go-daq/smbus"
)

const MuxEnable uint8 = 0x08
const MuxChannels = 8
const ChannelMask = 0x07

// All Smart Batteries have the same address so a Mux
// is required for multiple batteries to share the
// same bus.
type Mux struct {
	port  *smbus.Conn
	addr  uint8
	state uint8
}

// Maintain a table of all Muxes so we can ensure that
// only one is enabled at any given time.
var muxtab map[uint8]*Mux = make(map[uint8]*Mux)

// Return a new Mux given an I2C bus number and address
func NewMux(bus int, address int) (*Mux, error) {
	_, ok := muxtab[uint8(address)]
	if !ok {
		port, err := smbus.Open(bus, uint8(address))
		if err != nil {
			return nil, err
		}
		mux := Mux{port: port, addr: uint8(address), state: 0}
		mux.Disable()
		muxtab[uint8(address)] = &mux
	}

	return muxtab[uint8(address)], nil
}

// Select a mux channel (0-7)
func (m *Mux) SetChannel(c uint8) error {
	state := (c & ChannelMask) | MuxEnable
	if state == m.state {
		return nil
	}

	// Disable all other battery muxes
	for addr, mux := range muxtab {
		if addr != m.addr {
			mux.Disable()
		}
	}

	_, err := m.port.WriteByte(state)
	if err == nil {
		m.state = state
	}
	return err
}

// Return the current mux channel setting
func (m *Mux) GetChannel() (uint8, error) {
	var (
		err error
		b   [1]byte
	)
	_, err = m.port.Read(b[:])
	if err != nil {
		return 0, err
	}
	m.state = b[0]
	return m.state & ChannelMask, nil
}

// Disable the mux
func (m *Mux) Disable() error {
	_, err := m.port.WriteByte(0)
	if err == nil {
		m.state = 0
	}
	return err
}

// Close the bus connection to the Mux
func (m *Mux) Close() error {
	err := m.port.Close()
	delete(muxtab, m.addr)
	return err
}
