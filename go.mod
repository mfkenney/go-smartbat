module bitbucket.org/mfkenney/go-smartbat

go 1.13

require (
	github.com/go-daq/smbus v0.0.0-20180405160824-15ef727fb320
	github.com/mfkenney/go-embedded v0.0.0-20150410184215-34554ae00f36 // indirect
)
