package smartbat

import (
	"testing"
)

func TestNameFormatting(t *testing.T) {
	names := []string{"energy", "voltage", "charging_voltage"}
	regs := []string{"RemainingCapacity", "Voltage", "ChargingVoltage"}

	for i, s := range names {
		regname := lookupRegname(s)
		if regname != regs[i] {
			t.Errorf("Name mismatch %s != %s", regname, regs[i])
		}
	}
}
