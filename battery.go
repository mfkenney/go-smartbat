package smartbat

import (
	"errors"
	"fmt"
	"reflect"
	"sort"
	"strings"
	"time"
)

// Smart Battery connected to a Mux.
type Battery struct {
	mux      *Mux
	b        *SmartBattery
	mux_chan uint8
	nsamples int
	timeout  time.Duration
}

type key struct {
	sel     int
	regname string
}

// Battery data record
type Record struct {
	Id   int
	Data map[string]interface{}
}

// Reader to sample the battery state
type Reader struct {
	b       *Battery
	methods map[string]reflect.Value
}

// Map Battery variable names to register names
var registers map[string]string

// Map Battery register names to interface methods
var methods map[key]reflect.Value

// Errors
var (
	SmbusIo = errors.New("SMbus I/O error")
	BadVar  = errors.New("Invalid battery variable")
)

func init() {
	registers = map[string]string{
		"s/n":         "SerialNumber",
		"energy":      "RemainingCapacity",
		"cycles":      "CycleCount",
		"status":      "BatteryStatus",
		"rel_charge":  "RelativeStateOfCharge",
		"avg_current": "AverageCurrent",
		"chg_current": "ChargingCurrent"}
	methods = make(map[key]reflect.Value)
}

// Given a battery register name, return the Battery interface
// method used to access it.
func lookupMethod(b *Battery, regname string) reflect.Value {
	k := key{int(b.Selector()), regname}
	method, ok := methods[k]
	if !ok {
		method = reflect.ValueOf(b).MethodByName(regname)
		if method.IsValid() {
			methods[k] = method
		}
	}
	return method
}

// Given a Battery variable name, return the corresponding register name.
func lookupRegname(name string) string {
	regname, ok := registers[name]
	if !ok {
		// If the mapping is not already present, use the simple
		// rule of mapping underscore separated substrings to
		// CamelCase:
		//
		//    foo_bar_baz -> FooBarBaz
		//
		parts := []string{}
		for _, s := range strings.Split(name, "_") {
			parts = append(parts, strings.Title(s))
		}
		regname = strings.Join(parts, "")
		registers[name] = regname
	}
	return regname
}

// Return a new Battery on the specified bus and multiplexor (Mux).
// The "selector" is a bitmask where the upper 8-bits are the Mux
// address and the lower 8-bits are the Mux channel.
func NewBattery(bus int, selector uint16) (*Battery, error) {
	b, err := New(bus)
	if err != nil {
		return nil, fmt.Errorf("battery: %w", err)
	}
	m, err := NewMux(bus, int(selector>>8))
	if err != nil {
		return nil, fmt.Errorf("mux %02X: %w", selector>>8, err)
	}

	bat := Battery{
		mux:      m,
		b:        b,
		nsamples: 7,
		timeout:  time.Second,
		mux_chan: uint8(selector & 0xff)}
	err = m.SetChannel(bat.mux_chan)
	if err != nil {
		return nil, fmt.Errorf("select mux channel: %w", err)
	}
	err = b.Initialize()
	if err != nil {
		// Try again ...
		err = b.Initialize()
		if err != nil {
			return nil, fmt.Errorf("Battery %04X init: %w", selector, err)
		}
	}

	return &bat, nil
}

func (bat *Battery) sel() error {
	return bat.mux.SetChannel(bat.mux_chan)
}

func (bat *Battery) readReg(reg BatteryReg) (uint16, error) {
	var x sort.IntSlice
	var err error
	bat.sel()
	t0 := time.Now()
	// Take nsamples readings and return the median
	for x.Len() < bat.nsamples {
		if time.Now().Sub(t0) > bat.timeout {
			break
		}

		y, err := bat.b.readReg(reg)
		if err != nil {
			continue
		}
		x = append(x, int(y))
	}

	if x.Len() == 0 {
		return 0, fmt.Errorf("register %02x read: %w", reg, err)
	}

	x.Sort()
	return uint16(x[len(x)/2]), nil
}

// Return the Battery selector (Mux address and channel)
func (bat *Battery) Selector() uint16 {
	return uint16(bat.mux.addr)<<8 | uint16(bat.mux_chan)
}

// Return battery voltage in volts
func (bat *Battery) Voltage() (float32, error) {
	val, err := bat.readReg(Voltage)
	if err != nil {
		return 0., err
	}
	return float32(val) / 1000., nil
}

// Return battery current in amps. Negative current is flow out
// of the battery and positive current is flow in.
func (bat *Battery) Current() (float32, error) {
	val, err := bat.readReg(Current)
	if err != nil {
		return 0., err
	}
	return float32(int16(val)) / 1000., nil
}

// Return average current (in amps) over the past minute.
func (bat *Battery) AverageCurrent() (float32, error) {
	val, err := bat.readReg(AverageCurrent)
	if err != nil {
		return 0., err
	}
	return float32(int16(val)) / 1000., nil
}

// Return the requested charging current in amps
func (bat *Battery) ChargingCurrent() (float32, error) {
	val, err := bat.readReg(ChargingCurrent)
	if err != nil {
		return 0., err
	}
	return float32(int16(val)) / 1000., nil
}

// Return battery temperature in degrees C
func (bat *Battery) Temperature() (float32, error) {
	val, err := bat.readReg(Temperature)
	if err != nil {
		return 0., err
	}
	return float32(val)/10. - 273.15, nil
}

// Return remaining capacity in Watt-hours
func (bat *Battery) RemainingCapacity() (float32, error) {
	val, err := bat.readReg(RemainingCapacity)
	if err != nil {
		return 0., err
	}
	return float32(val / 100.), nil
}

// Return full-charge capacity in Watt-hours
func (bat *Battery) FullChargeCapacity() (float32, error) {
	val, err := bat.readReg(FullChargeCapacity)
	if err != nil {
		return 0., err
	}
	return float32(val / 100.), nil
}

// Return relative charge in percent
func (bat *Battery) RelativeStateOfCharge() (uint16, error) {
	return bat.readReg(RelativeStateOfCharge)
}

// Return max measurement error in percent
func (bat *Battery) MaxError() (uint16, error) {
	return bat.readReg(MaxError)
}

// Return battery status code
func (bat *Battery) BatteryStatus() (uint16, error) {
	return bat.readReg(BatteryStatus)
}

// Return battery-pack status code
func (bat *Battery) PackStatus() (uint16, error) {
	return bat.readReg(PackStatus)
}

// Return cycle count
func (bat *Battery) CycleCount() (uint16, error) {
	return bat.readReg(CycleCount)
}

// Return serial number
func (bat *Battery) SerialNumber() (uint16, error) {
	return bat.readReg(SerialNumber)
}

// Return a Reader instance that when read will return the current
// state of the specified battery variables.
func NewReader(bus int, selector uint16,
	varnames []string) *Reader {
	b, err := NewBattery(bus, selector)
	if err != nil {
		return nil
	}

	br := Reader{b: b, methods: make(map[string]reflect.Value)}

	for _, v := range varnames {
		method := lookupMethod(br.b, lookupRegname(v))
		if method.IsValid() {
			br.methods[v] = method
		}
	}

	return &br
}

// Return a battery data record.
func (br *Reader) Read() (Record, error) {
	var (
		err error
	)

	arg := []reflect.Value{}
	rec := Record{
		Id:   int(br.b.Selector()),
		Data: make(map[string]interface{})}

	for name, method := range br.methods {
		results := method.Call(arg)
		rec.Data[name] = results[0].Interface()
		if !results[1].IsNil() {
			err = results[1].Interface().(error)
			if err != nil {
				return rec, fmt.Errorf("Battery %04X: %w", br.b.Selector(), err)
			}
		}
	}
	return rec, nil
}

// Print a data record
func (r Record) String() string {
	return fmt.Sprintf("%04x %#v", r.Id, r.Data)
}
